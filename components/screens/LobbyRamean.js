import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import React from "react";
import CloseSVG from "../../assets/image/icon/Close.svg";
import SoundSVG from "../../assets/image/icon/VolumeUp.svg";

const LobbyRamean = ({ navigation }) => {
  return (
    <ImageBackground
      source={require("../../assets/image/background/Background.png")}
      style={styles.container}
    >
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <CloseSVG />
        </TouchableOpacity>

        {/* Volume Icon */}
        <View style={styles.headerRightContainer}>
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>

          <TouchableOpacity>
            <CloseSVG />
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

export default LobbyRamean;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height + 50,
  },
  headerContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 14,
    marginRight: 19,
    marginTop: 30,
  },
  headerRightContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
});
