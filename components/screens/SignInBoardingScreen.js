import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import React from "react";
import BigLogo from "../../assets/image/boarding/BigLogo.svg";
import NextArrow from "../../assets/image/arrow/NextArrow.svg";

const SignInBoardingScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      {/* Logo */}
      <BigLogo style={{ flex: 1, marginLeft: -103, marginTop: 116}} width= {466} height= {393} />

      {/* Welcome Text */}
      <View style={styles.welcomeContainer}>
        <Text style={styles.welcomeTitleText}>Halo Asep,</Text>
        <View style={styles.welcomeDescContainer}>
          <Text style={styles.welcomeDescText}>
            Kamu tidak hanya akan belajar membaca atau menghitung disini.
          </Text>
          <Text style={styles.welcomeDescText2}>
            Kami akan membantu kamu untuk mencapai tujuan dengan mempelajari apa
            yang kamu cintai, secara mandiri atau dengan cara kolaborasi.
          </Text>
        </View>

        {/* Go To Home */}
        <View style={styles.toHomeContainer}>
          <TouchableOpacity style={styles.toHomeButtonStyle} onPress={() => navigation.replace('homeScreen')}>
            <Text style={styles.toHomeText}>Go</Text>
            <NextArrow style={{ marginLeft: 11 }} />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SignInBoardingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    backgroundColor: "black",
  },
  welcomeContainer: {
    flex: 1,
    marginLeft: 28,
  },
  welcomeTitleText: {
    fontFamily: "avenir-next-demibold",
    color: "white",
    fontSize: 22,
  },
  welcomeDescContainer: {
    marginTop: 8,
  },
  welcomeDescText: {
    color: "white",
    fontSize: 16,
    fontFamily: "avenir-next-reguler",
  },
  welcomeDescText2: {
    color: "white",
    marginTop: 10,
    fontSize: 16,
    fontFamily: "avenir-next-reguler",
  },
  toHomeContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  toHomeButtonStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  toHomeText: {
    fontFamily: "avenir-next-demibold",
    fontSize: 20,
    color: "#6D75F6",
  },
});
