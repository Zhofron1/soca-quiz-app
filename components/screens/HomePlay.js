import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Switch,
  FlatList,
  Image,
  Alert 
} from "react-native";
import React, { useState, useEffect } from "react";
import CloseSVG from "../../assets/image/icon/Close.svg";
import SoundSVG from "../../assets/image/icon/VolumeUp.svg";
import PlayModal from "../views/PlayModal";

const HomePlay = ({ navigation, route }) => {
  const [quizId, setQuizId] = useState(route.id);
  const [username, setUsername] = useState(null)
  const [isEnabled, setIsEnabled] = useState(false);
  const [indexSpace, setIndexSpace] = useState(null);
  const [indexMusic, setIndexMusic] = useState(false);

  // modal status
  const [modalVisibleStatus, setModalVisibleStatus] = useState(true);

  const [dataSpace, setDataSpace] = useState([
    {
      id: 1,
      name: "Astronot",
      image: require("../../assets/image/background/Bg-Space.png"),
    },
    {
      id: 2,
      name: "Camping",
      image: require("../../assets/image/background/Bg-Camp.png"),
    },
    {
      id: 3,
      name: "Cafe",
      image: require("../../assets/image/background/Bg-Cafe.png"),
    },
    {
      id: 4,
      name: "Beach",
      image: require("../../assets/image/background/Bg-Beach.png"),
    },
    {
      id: 5,
      name: "Air Terjun",
      image: require("../../assets/image/background/Bg-Lake.png"),
    },
  ]);

  const [dataMusic, setDataMusic] = useState([
    {
      id: 1,
      name: "Metal",
      image: require("../../assets/image/background/Metal.png"),
    },
    {
      id: 2,
      name: "Classic",
      image: require("../../assets/image/background/Classical.png"),
    },
    {
      id: 3,
      name: "Rock",
      image: require("../../assets/image/background/Rock.png"),
    },
    {
      id: 4,
      name: "Beat",
      image: require("../../assets/image/background/Beat.png"),
    },
    {
      id: 5,
      name: "Techno",
      image: require("../../assets/image/background/Techno.png"),
    },
  ]);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const goToLobbyHandler = () => {
    if(username === null || username === ""){
      Alert.alert("Username Tidak boleh kosong")
    } else {
      navigation.replace("LobbyRamean", username)
    }
  }

  return (
    <ImageBackground
      source={require("../../assets/image/background/Background.png")}
      style={styles.container}
    >
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <CloseSVG />
        </TouchableOpacity>

        {/* Volume Icon */}
        <TouchableOpacity>
          <SoundSVG />
        </TouchableOpacity>
      </View>

      {/* Input Name */}
      <View style={styles.nameContainer}>
        <Text style={styles.nameTitleText}>Nama Lengkap</Text>
        <TextInput placeholder="Heri Tipsi" style={styles.nameTextInputStyle} onChangeText={(val) => setUsername(val)} value={username} />
        <TouchableOpacity style={styles.submitNameButtonStyle} onPress={() => goToLobbyHandler()}>
          <Text style={styles.submitNameText}>Mulai</Text>
        </TouchableOpacity>
      </View>

      {/* Settings */}
      <Text style={styles.titleText}>Pengaturan</Text>

      <View style={styles.settingsContainer}>
        <Text style={styles.settingsText}>Audio</Text>
        <Switch
          trackColor={{ false: "gray", true: "#40BE45" }}
          thumbColor={"white"}
          ios_backgroundColor="grey"
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </View>

      {/* Space */}
      <View style={styles.spaceContainer}>
        {/* Choose Space */}
        <Text style={styles.settingsText}>Pilih Space</Text>

        {/* List Space */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 10 }}
          horizontal={true}
          data={dataSpace}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={styles.cardSpaceContainer}>
              <TouchableOpacity
                onPress={() => {
                  setIndexSpace(item.id);
                }}
              >
                <Image
                  source={item.image}
                  style={{
                    borderWidth: indexSpace === item.id ? 1.5 : 0,
                    borderColor: "#40BE45",
                  }}
                />
                <Text style={styles.spaceText}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          )}
        />

        {/* Music */}
        <Text style={styles.musicTitleText}>Pilih Music</Text>

        {/* List Music */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 11 }}
          horizontal={true}
          data={dataMusic}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={styles.cardSpaceContainer}>
              <TouchableOpacity
                onPress={() => {
                  setIndexMusic(item.id);
                }}
              >
                <Image
                  source={item.image}
                  style={{
                    borderWidth: indexMusic === item.id ? 1.5 : 0,
                    borderColor: "#40BE45",
                  }}
                />
                <Text style={styles.spaceText}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
      <PlayModal
        modalVisible={modalVisibleStatus}
        handleOnClose={() => setModalVisibleStatus(false)}
        handleOnSkip={() => setModalVisibleStatus(false)}
      />
    </ImageBackground>
  );
};

export default HomePlay;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height + 50,
  },
  headerContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 14,
    marginRight: 31,
    marginTop: 30,
  },
  nameContainer: {
    backgroundColor: "#1B1B1B",
    marginHorizontal: 31,
    marginTop: 37,
    paddingHorizontal: 27,
    paddingVertical: 16,
  },
  nameTitleText: {
    fontFamily: "avenir-next-reguler",
    fontSize: 12,
    color: "white",
  },
  nameTextInputStyle: {
    marginTop: 7,
    backgroundColor: "white",
    padding: 8,
    borderRadius: 2,
    marginBottom: 11,
  },
  submitNameButtonStyle: {
    backgroundColor: "#6D75F6",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    paddingVertical: 8,
  },
  submitNameText: {
    color: "white",
    fontFamily: "avenir-next-medium",
    fontSize: 14,
  },
  titleText: {
    fontFamily: "avenir-next-demibold",
    fontSize: 14,
    color: "white",
    marginLeft: 31,
    marginTop: 18,
  },
  settingsContainer: {
    backgroundColor: "#1B1B1B",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: 27,
    paddingVertical: 16,
    marginHorizontal: 31,
    borderRadius: 8,
  },
  settingsText: {
    fontFamily: "avenir-next-reguler",
    fontSize: 12,
    color: "white",
  },
  spaceContainer: {
    marginTop: 18,
    backgroundColor: "#1B1B1B",
    paddingLeft: 31,
    paddingTop: 18,
    paddingBottom: 20,
  },
  cardSpaceContainer: {
    marginLeft: 6,
    alignItems: "center",
    justifyContent: "center",
  },
  spaceText: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 10,
    alignSelf: "center",
    marginTop: 7,
  },
  musicTitleText: {
    fontFamily: "avenir-next-demibold",
    fontSize: 12,
    color: "white",
    marginTop: 15,
  },
});
