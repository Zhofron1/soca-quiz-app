import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import textStyles from '../styles/TextStyles';
import TopLeftCorner from '../../assets/TopLeftCorner.svg';
import TopRightCorner from '../../assets/TopRightCorner.svg';
import BottomLeftCorner from '../../assets/BottomLeftCorner.svg';
import BottomRightCorner from '../../assets/BottomRightCorner.svg';

const ScanQrTab = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    const getBarCodeScannerPermissions = async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    };

    getBarCodeScannerPermissions();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  if (hasPermission === null) {
    return <Text style={textStyles.semiBoldText}>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text style={textStyles.semiBoldText}>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <Text style={[textStyles.semiBoldText, { fontSize: 18, marginTop: 33, flex: 1 }]}>Join and have fun together!</Text>
      <View style={{ flex: 4, alignItems: 'center' }}>
        <BarCodeScanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} style={styles.cameraFrame} width={314} height={314}>
          <View style={styles.corner}>
            <TopLeftCorner style={{ marginStart: 36 }} />
            <TopRightCorner style={{ marginEnd: 36 }} />
          </View>
          <View style={styles.corner}>
            <BottomLeftCorner style={{ marginStart: 36 }} />
            <BottomRightCorner style={{ marginEnd: 36 }} />
          </View>
        </BarCodeScanner>
        <Text style={[textStyles.semiBoldText, { marginTop: 22 }]}>Align the QR Code within the frame</Text>
        {/* {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />} */}
      </View>
    </View>
  );
};

export default ScanQrTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
    alignItems: 'center',
  },
  cameraFrame: {
    width: 314,
    height: 314,
    justifyContent: 'space-between',
  },
  corner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
