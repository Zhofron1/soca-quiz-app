import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity, TouchableHighlight, TextInput } from 'react-native';
import React, { useState, useRef } from 'react';
import LeftArrow from '../../assets/LeftArrow.svg';
import SocaLogo from '../../assets/SocaLogo.svg';
import OtpInputLayout from '../views/OtpInputLayout';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';

const OtpVerificationScreen = ({ navigation }) => {
  const pin1Ref = useRef(null);
  const pin2Ref = useRef(null);
  const pin3Ref = useRef(null);
  const pin4Ref = useRef(null);
  const pin5Ref = useRef(null);
  const pin6Ref = useRef(null);

  const [pin1, setPin1] = useState('');
  const [pin2, setPin2] = useState('');
  const [pin3, setPin3] = useState('');
  const [pin4, setPin4] = useState('');
  const [pin5, setPin5] = useState('');
  const [pin6, setPin6] = useState('');
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TouchableHighlight onPress={() => navigation.navigate('forgotPasswordScreen')}>
          <View style={styles.back}>
            <LeftArrow />
            <Text style={[textStyles.semiBoldText, { fontSize: 20, marginStart: 4 }]}>Kembali</Text>
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={[textStyles.semiBoldText, { fontSize: 24, marginTop: 32 }]}>Lupa Password</Text>
        <Text style={[textStyles.normalText, { fontSize: 14, marginTop: 8, marginBottom: 77, marginHorizontal: 75, textAlign: 'center' }]}>Masukkan kode verifikasi yang dikirimkan ke email kamu</Text>
        <View style={{ flexDirection: 'row' }}>
          <TextInput
            ref={pin1Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin1) => {
              setPin1(pin1);
              if (pin1 !== '') {
                pin2Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin2Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin2) => {
              setPin2(pin2);
              if (pin2 !== '') {
                pin3Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin3Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin3) => {
              setPin3(pin3);
              if (pin3 !== '') {
                pin4Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin4Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin4) => {
              setPin4(pin4);
              if (pin4 !== '') {
                pin5Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin5Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin5) => {
              setPin5(pin5);
              if (pin5 !== '') {
                pin6Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin6Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={'number-pad'}
            onChange={(pin6) => {
              setPin6(pin6);
            }}
          />
        </View>
        <ButonView text="Kirim" onPress={() => navigation.navigate('newPasswordScreen')} />

        {/* Resend */}
        <View style={styles.sendCodeStyle}>
          <Text style={styles.sendCodeText}>Belum menerima kode?</Text>
          <TouchableOpacity>
            <Text style={styles.sendCodeSpecialText}>Kirim Ulang</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View>
    </View>
  );
};

export default OtpVerificationScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footer: {
    backgroundColor: '#000',
    marginBottom: 26,
  },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  formStyle: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    textAlign: 'center',
    borderColor: '#8C8C8C',
    borderWidth: 1,
    borderRadius: 6,
    width: 41,
    height: 48,
    marginStart: 6,
    marginEnd: 6,
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
  textPressable: {
    fontFamily: 'avenir-next-demibold',
    color: '#6D75F6',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
  },
  sendCodeStyle: {
    marginTop: 49,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sendCodeText: {
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    fontSize: 12,
  },
  sendCodeSpecialText: {
    fontFamily: 'avenir-next-demibold',
    marginLeft: 4,
    color: '#6D75F6',
    fontSize: 12,
  },
});
