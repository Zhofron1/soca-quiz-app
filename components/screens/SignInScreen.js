import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import React from 'react';
import SocaLogo from '../../assets/SocaLogo.svg';
import TextInputLayout from '../views/TextInputLayout';
import ContinueAppleButton from '../views/ContinueAppleButton';
import ContinueGoogleButton from '../views/ContinueGoogleButton';
import ButonView from '../views/ButonView';

const SignInScreen = ({ navigation }) => {
  return (
    <ScrollView style={styles.scrolContainer}>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={styles.title}>Login</Text>
        <ContinueAppleButton text="Continue with Apple" />
        <ContinueGoogleButton text="Continue with Google" />
        <Text style={[styles.normalText, { marginTop: 20 }]}>atau masuk dengan</Text>
        <TextInputLayout label="Email" placeholder="Masukkan email kamu" keyboardType="email-address" />
        <TextInputLayout label="Password" placeholder="Buat password" secureTextEntry={true} />
        <View style={styles.forgotPasswordView}>
          <TouchableOpacity onPress={() => navigation.navigate('forgotPasswordScreen')}>
            <Text style={styles.textPressable}>Lupa password?</Text>
          </TouchableOpacity>
        </View>
        <ButonView text="Masuk" onPress={() => navigation.navigate('signInBoardingScreen')} />
        <View style={styles.toSignup}>
          <Text style={styles.normalText}>Belum punya akun?</Text>
          <TouchableOpacity style={{ marginStart: 4 }} onPress={() => navigation.navigate('signupScreen')}>
            <Text style={styles.textPressable}>Daftar disini</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.footer}>Copyright © 2023 Soca.ai All Reserved</Text>
      </View>
    </ScrollView>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  scrolContainer: {
    flex: 1,
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  container: {
    alignItems: 'center',
    marginTop: 155,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 24,
    marginTop: 32,
  },
  normalText: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    fontSize: 12,
    textAlign: 'center',
  },
  textPressable: {
    fontFamily: 'avenir-next-demibold',
    color: '#6D75F6',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
    alignSelf: 'center',
  },
  toSignup: {
    flexDirection: 'row',
    marginTop: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 110,
  },
  forgotPasswordView: {
    marginEnd: 38,
    marginTop: 20,
    alignSelf: 'flex-end',
  },
});
