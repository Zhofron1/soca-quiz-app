import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import SocaLogo from '../../assets/SocaLogo.svg';
import SearchIcon from '../../assets/Search.svg';
import NotificationIcon from '../../assets/Notification.svg';

const TopBarView = () => {
  return (
    <View style={styles.container}>
      <SocaLogo width={87.44} height={21.35} />
      <View style={styles.iconAction}>
        <SearchIcon style={{ marginEnd: 22 }} />
        <NotificationIcon />
      </View>
    </View>
  );
};

export default TopBarView;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 70,
    backgroundColor: '#000',
    margin: 24,
    justifyContent: 'space-between',
  },
  iconAction: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
});
