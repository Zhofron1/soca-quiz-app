import { StyleSheet, Text, View, Modal, TouchableOpacity } from "react-native";
import React, {useState} from "react";
import { BlurView } from "expo-blur";
import ContinueGoogleButton from "./ContinueGoogleButton";

const PlayModal = ({ modalVisible,   handleOnClose, handleOnSignIn, handleOnSkip }) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={handleOnClose}
    >
      <BlurView intensity={100} tint="dark" style={styles.modalContainer}>
        <View style={styles.bodyContainer}>
          <Text style={styles.textStyle}>
            Please sign in using a Google account
          </Text>
          <ContinueGoogleButton text="Continue with Google" borderRadius={25} />
          <TouchableOpacity onPress={handleOnSkip}>
            <Text style={styles.skipTextStyle}>Skip</Text>
          </TouchableOpacity>
        </View>
      </BlurView>
    </Modal>
  );
};

export default PlayModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  bodyContainer: {
    height: 235,
    width: 414,
    backgroundColor: "#1F1F1F",
    alignContent: "center",
    justifyContent: "center",
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
  },
  textStyle: {
    alignSelf: "center",
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontSize: 18,
    marginBottom: 35,
    marginTop: 50,
  },
  googleButtonStyle: {
    backgroundColor: "white",
    alignContent: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  buttonText: {
    fontFamily: "avenir-next-reguler",
    fontSize: 15,
    borderRadius: 25,
  },
  skipTextStyle: {
    alignSelf: "center",
    fontFamily: "avenir-next-reguler",
    fontSize: 17,
    color: "#8C8C8C",
    marginTop: 21,
    marginBottom: 33,
  },
});
