// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import AppLoading from 'expo-app-loading';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignUpScreen from './components/screens/SignUpScreen';
import SignInScreen from './components/screens/SignInScreen';
import ForgotPasswordScreen from './components/screens/ForgotPasswordScreen';
import OtpVerificationScreen from './components/screens/OtpVerificationScreen';
import NewPasswordScreen from './components/screens/NewPasswordScreen';
import HomeScreen from './components/screens/HomeScreen';
import JoinScreen from './components/screens/JoinScreen';
import PlayNavigation from './components/routes/PlayNavigation';

const Stack = createStackNavigator();

const getFont = () => {
  return Font.loadAsync({
    'avenir-next-reguler': require('./assets/fonts/AvenirNextLTPro-Regular.otf'),
    'avenir-next-demibold': require('./assets/fonts/AvenirNext-DemiBold.ttf'),
    'avenir-next-medium': require('./assets/fonts/AvenirNextCyr-Medium.ttf'),
    'avenir-next-it': require('./assets/fonts/AvenirNextLTPro-It.otf'),
    'avenir-next-bold': require('./assets/fonts/AvenirNextLTPro-Bold.otf'),
  });
};

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  if (fontsLoaded) {
    return (
      <NavigationContainer>
        {/* <BottomNavigation /> */}
        <PlayNavigation />
        {/* <Stack.Navigator> */}
        {/* <Stack.Screen name="signinScreen" component={SignInScreen} options={{ headerShown: false }} />
          <Stack.Screen name="signupScreen" component={SignUpScreen} options={{ headerShown: false }} />
          <Stack.Screen name="forgotPasswordScreen" component={ForgotPasswordScreen} options={{ headerShown: false }} />
          <Stack.Screen name="otpVerificationScreen" component={OtpVerificationScreen} options={{ headerShown: false }} />
          <Stack.Screen name="newPasswordScreen" component={NewPasswordScreen} options={{ headerShown: false }} /> */}
        {/* <Stack.Screen name="signInBoardingScreen" component={SignInBoardingScreen} options={{ headerShown: false }}/> */}
        {/* <Stack.Screen name="homeScreen" component={HomeScreen} options={{ headerShown: false }} /> */}
        {/* </Stack.Navigator> */}
      </NavigationContainer>
    );
  } else {
    return <AppLoading startAsync={getFont} onFinish={() => setFontsLoaded(true)} onError={() => console.log('error')} />;
  }
}
